#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/platform/bootdevice/by-name/recovery:35113808:09586f5fd425b35fbdf5819c27a844ba86a73bb1; then
  applypatch  EMMC:/dev/block/platform/bootdevice/by-name/boot:9929552:7d1b0eb5ecb90cc54b7e274ce421b2269cadd721 EMMC:/dev/block/platform/bootdevice/by-name/recovery 09586f5fd425b35fbdf5819c27a844ba86a73bb1 35113808 7d1b0eb5ecb90cc54b7e274ce421b2269cadd721:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
